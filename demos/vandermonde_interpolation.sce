// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

mprintf("Use Vandermonde matrix to perform\n")
mprintf("Lagrange interpolation.\n")
mprintf("See http://en.wikipedia.org/wiki/Lagrange_polynomial\n")
// The X points
x = [
-1.5
-0.75
0.
0.75
1.5
];
// The Y values
y = [
-14.1014
-0.931596
0.
0.931596
14.1014
];
// The matrix
V = makematrix_vandermonde(x)
// The coefficients of the interpolating
// polynomial
u = V\y
// Compute values of the interpolating polynomial
xI = linspace(-2,2,1000);
n = size(x,"*");
V = makematrix_vandermonde(xI,n);
yI = V * u;
// Draw the points
scf();
plot(x,y,"b*");
plot(xI,yI,"r-");
legend(["Data","Lagrange Polynomial"]);
xtitle("Lagrange Interpolation","X","Y");

//
// Load this script into the editor
//
filename = "vandermonde_interpolation.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );

