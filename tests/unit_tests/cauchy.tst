// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

computed = makematrix_cauchy ( 1:5 , 6:10 );
expected = [
   0.142857142857143   0.125000000000000   0.111111111111111   0.100000000000000   0.090909090909091
   0.125000000000000   0.111111111111111   0.100000000000000   0.090909090909091   0.083333333333333
   0.111111111111111   0.100000000000000   0.090909090909091   0.083333333333333   0.076923076923077
   0.100000000000000   0.090909090909091   0.083333333333333   0.076923076923077   0.071428571428571
   0.090909090909091   0.083333333333333   0.076923076923077   0.071428571428571   0.066666666666667
];
assert_checkalmostequal ( computed, expected, 100 * %eps );
//
computed = makematrix_cauchy ( 1:5 );
expected = [
    0.5          0.3333333    0.25         0.2          0.1666667
    0.3333333    0.25         0.2          0.1666667    0.1428571
    0.25         0.2          0.1666667    0.1428571    0.125
    0.2          0.1666667    0.1428571    0.125        0.1111111
    0.1666667    0.1428571    0.125        0.1111111    0.1
];
assert_checkalmostequal ( computed, expected, 1.e-6 );

