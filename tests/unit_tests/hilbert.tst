// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

computed = makematrix_hilbert ( 5 );
expected = [
1. 1./2. 1./3. 1./4. 1./5.
1./2. 1./3. 1./4. 1./5. 1./6.
1./3. 1./4. 1./5. 1./6. 1./7.
1./4. 1./5. 1./6. 1./7. 1./8.
1./5. 1./6. 1./7. 1./8. 1./9.
]
assert_checkalmostequal ( computed, expected, %eps )

