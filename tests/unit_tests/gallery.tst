// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->


// Cauchy
computed = makematrix_gallery("cauchy" , 1:5 );
expected = [
    0.5000    0.3333    0.2500    0.2000    0.1667
    0.3333    0.2500    0.2000    0.1667    0.1429
    0.2500    0.2000    0.1667    0.1429    0.1250
    0.2000    0.1667    0.1429    0.1250    0.1111
    0.1667    0.1429    0.1250    0.1111    0.1000
];
assert_checkalmostequal ( computed, expected, 1.e-3 );
// Cauchy
computed = makematrix_gallery("cauchy" , 1:5 , 2:6 );
expected = [
    0.3333    0.2500    0.2000    0.1667    0.1429
    0.2500    0.2000    0.1667    0.1429    0.1250
    0.2000    0.1667    0.1429    0.1250    0.1111
    0.1667    0.1429    0.1250    0.1111    0.1000
    0.1429    0.1250    0.1111    0.1000    0.0909
];
assert_checkalmostequal ( computed, expected, 1.e-3 );
// circul
computed = makematrix_gallery("circul" , 1:5 );
expected = [
     1     2     3     4     5
     5     1     2     3     4
     4     5     1     2     3
     3     4     5     1     2
     2     3     4     5     1
];
assert_checkalmostequal ( computed, expected, %eps );
// circul
x=[
1
2
3
];
computed = makematrix_gallery("circul" , x );
expected = [
     1     2     3
     3     1     2
     2     3     1
];
assert_checkalmostequal ( computed, expected, %eps );
// makematrix_gallery(3)
E = [
  -149   -50  -154
   537   180   546
   -27    -9   -25
   ];
A = makematrix_gallery(3);
assert_checkalmostequal ( A , E , %eps );
// makematrix_gallery(4)
E = [
        10     7     8     7
         7     5     6     5
         8     6    10     9
         7     5     9    10
   ];
A = makematrix_gallery(4);
assert_checkalmostequal ( A , E , %eps );
// makematrix_gallery(5)
E = [
          -9          11         -21          63        -252
          70         -69         141        -421        1684
        -575         575       -1149        3451      -13801
        3891       -3891        7782      -23345       93365
        1024       -1024        2048       -6144       24572
   ]
A = makematrix_gallery(5);
assert_checkalmostequal ( A , E , %eps );
// makematrix_gallery(8)
E = [
          611.  196. -192.  407.   -8.  -52.  -49.   29.
          196.  899.  113. -192.  -71.  -43.   -8.  -44.
         -192.  113.  899.  196.   61.   49.    8.   52.
          407. -192.  196.  611.    8.   44.   59.  -23.
           -8.  -71.   61.    8.  411. -599.  208.  208.
          -52.  -43.   49.   44. -599.  411.  208.  208.
          -49.   -8.    8.   59.  208.  208.   99. -911.
           29.  -44.   52.  -23.  208.  208. -911.   99.
           ];
A = makematrix_gallery(8);
assert_checkalmostequal ( A , E , %eps );

