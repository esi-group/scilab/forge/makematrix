// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->


computed = makematrix_hadamard ( 1 );
expected = [1];
assert_checkalmostequal ( computed , expected , %eps );

computed = makematrix_hadamard ( 2 );
expected = [
1 1
1 -1];
assert_checkalmostequal ( computed , expected , %eps );

computed = makematrix_hadamard ( 4 );
expected = [
1    1    1    1
1   -1    1   -1
1    1   -1   -1
1   -1   -1    1
];
assert_checkalmostequal ( computed , expected , %eps );


