// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_hankel ( n , x )
    //   Returns the Hankel matrix
    //
    // Calling Sequence
    // A = makematrix_hankel ( n , x )
    //
    // Parameters
    //  n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    //  x : a m-by-1 or 1-by-m matrix of doubles, the inputs, with m==2*n-1
    //  A : a n-by-n matrix of doubles, the Hankel matrix
    //
    // Description
    //   Returns the Hankel matrix of size n.
	//   The positive diagonals of the Hankel matrix are constant.
	//
	//   For example, consider n=5 and m=9.
	//   If <literal>x=[a b c d e f g h i]</literal>,
	//   then the associated 5-by-5 Hankel matrix is
	//  <screen>
	//  A = [
	//  a b c d e
	//  b c d e f
	//  c d e f g
	//  d e f g h
	//  e f g h i
	//  ]
	//  </screen>
	//
	//  The Hankel matrix satisfies
	//  <literal>
	//  A(i,j) == A(i-1,j+1)
	//  </literal>
	//
	// See makematrix_toeplitz for a similar matrix construction.
    //
    // Examples
    //    n = 5;
    //    x = [1 2 3 4 5 6 7 8 9];
    //    A = makematrix_hankel ( n , x )
    //    expected = [
    //    1  2  3  4  5
    //    2  3  4  5  6
    //    3  4  5  6  7
    //    4  5  6  7  8
    //    5  6  7  8  9
    //    ];
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  http://en.wikipedia.org/wiki/Hankel_matrix
	//
	// See also
	// makematrix_circul
	// makematrix_toeplitz
    //

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_hankel" , rhs , 2 : 2 )
    apifun_checklhs ( "makematrix_hankel" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_hankel" , n , "n" , 1 , "constant" )
    apifun_checktype ( "makematrix_hankel" , x , "x" , 2 , "constant" )
	//
    apifun_checkscalar ( "makematrix_hankel" , n , "n" , 1 )
	apifun_checkvector ( "makematrix_hankel" , x , "x" , 2 )
	//
    apifun_checkflint ( "makematrix_hankel" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_hankel" , n , "n" , 1 , 1 )
    //
	x = x(:)'
    A = zeros(n,n);
    nx1 = size(x,1)
    nx2 = size(x,2)
    if nx2 <> 2 * n - 1 then
        error(sprintf("The second dimension of the input row vector x is %d and not %d as expected.",nx1,2*n-1))
    end
    if nx1 <> 1 then
        error(sprintf("The first dimension of the input row vector x is %d and not 1 as expected.",nx1))
    end
    first = 1;
    last = n;
    for i = 1:n
        A(i,1:n) = x(first:last)
        first = first + 1
        last = last + 1
    end
endfunction

