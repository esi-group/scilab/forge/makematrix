// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_hilbert ( n )
    //   Returns the hilbert matrix
    //
    // Calling Sequence
    //   A = makematrix_hilbert ( n )
    //
    // Parameters
    //  n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    //  A : a n-by-n matrix of doubles, integer values
    //
    // Description
    //   Returns the hilbert matrix of size n
    //
	//   For i,j=1,2,...,n, we have:
    //   <literal>
    //   A(i,j) = 1 / (i + j - 1)
    //   </literal>
    //
    //   The Hilbert matrix is symetric, positive definite and ill conditioned.
	//
	//   cond(makematrix_hilbert ( n )) grows like exp(3.5*n).
    //
    // Examples
    // A = makematrix_hilbert ( 5 )
	//
	// // In the following example, we compute the conditionning of
	// // Hilbert's matrix for various values of n.
    // cond(makematrix_hilbert ( 2 ))
    // cond(makematrix_hilbert ( 4 ))
    // cond(makematrix_hilbert ( 6 ))
    // cond(makematrix_hilbert ( 8 ))
    // cond(makematrix_hilbert ( 10 ))
	//
	// // In the following example, we compare
	// // the estimated condition number of Hilbert's
	// // matrix for increasing values of n with its theoretical value.
	// nv = 1:30;
	// for n = nv
  	// c(n) = log(cond(makematrix_hilbert(n)));
  	// e(n) = log(exp(3.5*n));
	// end
	// scf();
	// plot ( nv , c , "bo" );
	// plot ( nv , e , "r-" );
	// legend(["cond" "Theory"]);
    // xtitle("Conditionning of Hilbert matrix",...
	//     "N","Condition Number");
	//
	// // Analyse the condition number.
	// // Compare exact solution from the inverse Hilbert
	// // matrix with backslash operator.
	// // See how Scilab switches from Gaussian elimination
	// // with pivoting to a least squares solution.
	// err = zeros(14,1);
	// co = zeros(14,1);
	// for n = 2:15
	//     A = makematrix_hilbert(n);
	//     b = ones(n,1);
	// 	Ainv = makematrix_invhilbert(n);
	// 	x = A\b;
	// 	exact = Ainv*b;
	//     err(n-1) = norm(x-exact)/norm(exact);
	//     co(n-1) = cond(A);
	// end
	// h = scf();
	// plot(2:15,err,"r")
	// plot(2:15,co,"x");
	// h.children.log_flags="nln";
	// xtitle("Sensitivity of Hilbert matrix","n");
	// legend(["Relative error","Condition number"]);
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  http://en.wikipedia.org/wiki/Hilbert_matrix
    //

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_hilbert" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_hilbert" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_hilbert" , n , "n" , 1 , "constant" )
	//
    apifun_checkscalar ( "makematrix_hilbert" , n , "n" , 1 )
	//
    apifun_checkflint ( "makematrix_hilbert" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_hilbert" , n , "n" , 1 , 1 )
    //
    i = (1:n)'
    i = i(:,ones(1,n))
    j = 1:n
    j = j(ones(n,1),:)
    A = 1 ./ (i + j - 1.)
endfunction

