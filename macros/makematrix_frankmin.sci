// Copyright (C) 2011 - Michael Baudin
//  Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_frankmin ( n )
    //   Returns the Frank-min matrix
    // Arguments, input
    //
    // Calling Sequence
    //  A = makematrix_frankmin ( n )
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    // A : a n-by-n matrix of doubles, the Frank-min matrix
    //
    // Description
    //   Returns the Frank-min matrix of size n.
    //
  	//   For i=1,2,...,n, we have :
    //   <literal>
    //   A(i,j) = min(i,j)
    //   </literal>
    //
    //   "A reasonably well behaved matrix." see Nash below.
    //
    // Examples
    // A = makematrix_frankmin ( 5 )
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  J.C. Nash, Compact Numerical Methods for Computers: Linear, Algebra and Function Minimisation, second edition, Adam Hilger, Bristol, 1990 (Appendix 1).
    //

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_frankmin" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_frankmin" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_frankmin" , n , "n" , 1 , "constant" )
	//
    apifun_checkscalar ( "makematrix_frankmin" , n , "n" , 1 )
	//
    apifun_checkflint ( "makematrix_frankmin" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_frankmin" , n , "n" , 1 , 1 )
    //
    x = (1:n)
    y = x'
    x = x(ones(n,1),:)
    y = y(:,ones(n,1))
    A = min(x,y)
endfunction

