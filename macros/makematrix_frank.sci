// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//  Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_frank ( varargin )
    // Returns Frank matrix.
    //
    // Calling Sequence
    //   A = makematrix_frank ( n )
    //   A = makematrix_frank ( n , k )
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    // k : a 1-by-1 matrix of doubles, integer value, available values are k=0 or k=1 (default k=0)
    // A : a n-by-n matrix of doubles, the Frank matrix
    //
    // Description
    //   Returns the Frank matrix of order n.
	//   It is upper Hessenberg with determinant 1.
    //   The parameter k = 0 is the default.
	//   If k = 1 the elements are reflected about the anti-diagonal (1,n)--(n,1).
    //
    //   The Frank matrix has all positive eigenvalues and they occur in reciprocal pairs
    //   (so that 1 is an eigenvalue if n is odd).
    //   The eigenvalues of the Frank matrix may be obtained in terms of the zeros of the
    //   Hermite polynomials.
    //   The floor(n/2) smallest eigenvalues of the Frank matrix are ill conditioned,
    //   the more so for bigger n.
    //
    //   det(makematrix_frank(n)) is very different from 1 for large n.
	//   See Frank (1958) and Wilkinson (1960) for discussions.
    //
    //   This version incorporates improvements suggested by W. Kahan.
    //
    //   Eberlein (1971) and Varah (1986) give details of the eigensystem, as does
    //   Rutishauser (1966).
    //
    // Original author : Nicolas Higham, Scilab port : Michael Baudin, 2011
    //
    // Examples
    // A = makematrix_frank ( 5 )
    // A = makematrix_frank ( 5 , 1 )
	//
	// // In the following example, we compute the determinant of the
	// // Frank matrix for various values of n.
	// // For values of n greater than 10, the determinant computed
	// // with the det function is different from 1.
	// // Notice that, because of the use of extended precision on
	// // some processors, the result may differ from machine to machine
	// // (but the order of magnitude stays the same).
    // det(makematrix_frank ( 5 )')
    // det(makematrix_frank ( 10 )')
    // det(makematrix_frank ( 20 )')
    // det(makematrix_frank ( 50 )')
    // det(makematrix_frank ( 100 )')
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //   W.L. Frank, Computing eigenvalues of complex matrices by determinant evaluation and by methods of Danilewski and Wielandt, J. Soc. Indust. Appl. Math., 6 (1958), pp. 378-392 (see pp. 385, 388).
    //   Nicolas Higham, Accuracy and Stability of Numerical Algorithms, A Gallery of Test Matrices
    //   G.H. Golub and J.H. Wilkinson, Ill-conditioned eigensystems and the computation of the Jordan canonical form, SIAM Review, 18 (1976), pp. 578-619 (Section 13).
    //   H. Rutishauser, On test matrices, Programmation en Mathematiques Numeriques, Editions Centre Nat. Recherche Sci., Paris, 165, 1966, pp. 349-365.  Section 9.
    //   J.H. Wilkinson, Error analysis of floating-point computation, Numer. Math., 2 (1960), pp. 319-340 (Section 8).
    //   J.H. Wilkinson, The Algebraic Eigenvalue Problem, Oxford University Press, 1965 (pp. 92-93).
    //   P.J. Eberlein, A note on the matrices denoted by B_n, SIAM J. Appl. Math., 20 (1971), pp. 87-92.
    //   J.M. Varah, A generalization of the Frank matrix, SIAM J. Sci. Stat. Comput., 7 (1986), pp. 835-839.

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_frank" , rhs , 1 : 2 )
    apifun_checklhs ( "makematrix_frank" , lhs , 1 : 1 )
    //
    n = varargin(1)
	k = apifun_argindefault ( varargin , 2 , 0 )
	//
    apifun_checktype ( "makematrix_frank" , n , "n" , 1 , "constant" )
    apifun_checktype ( "makematrix_frank" , k , "k" , 2 , "constant" )
	//
    apifun_checkscalar ( "makematrix_frank" , n , "n" , 1 )
    apifun_checkscalar ( "makematrix_frank" , k , "k" , 2 )
	//
    apifun_checkflint ( "makematrix_frank" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_frank" , n , "n" , 1 , 1 )
    apifun_checkflint ( "makematrix_frank" , k , "k" , 2 )
    apifun_checkoption ( "makematrix_frank" , k , "k" , 2 , [0 1] )
	//
    p = n:-1:1
    A = triu( p( ones(n,1), :) - diag( ones(n-1,1), -1), -1 )
    // A = A(p,p)' reverses all the rows and columns of A
    if ( k <> 0 ) then
        A = A(p,p)'
    end
endfunction

