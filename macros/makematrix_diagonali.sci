// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_diagonali ( n )
    //   Returns a diagonal-i matrix
    //
    // Calling Sequence
    // A = makematrix_diagonali ( n )
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    // A : a n-by-n matrix of doubles, the Diagonal-i matrix
    //
    // Description
    //   Returns a matrix of size n with i on the diagonal.
    //
	//   For i=1,2,...,n, we have :
    //   <literal>
    //   A(i,i) = i
    //   </literal>
    //
    // Examples
    // A = makematrix_diagonali ( 5 )
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //  J.C. Nash, Compact Numerical Methods for Computers: Linear Algebra and Function Minimisation, second edition, Adam Hilger, Bristol, 1990 (Appendix 1).

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_diagonali" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_diagonali" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_diagonali" , n , "n" , 1 , "constant" )
	//
    apifun_checkscalar ( "makematrix_diagonali" , n , "n" , 1 )
	//
    apifun_checkflint ( "makematrix_diagonali" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_diagonali" , n , "n" , 1 , 1 )
    //
    A = diag(1:n)
endfunction

