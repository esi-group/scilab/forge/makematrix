// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_magic ( n )
    //   Returns the magic matrix
    //
    // Calling Sequence
    // A = makematrix_magic ( n )
    //
    // Parameters
    //  n : a 1-by-1 matrix of doubles, integer value, the size of the matrix to return
    //  A : a n-by-n matrix of doubles, integer values
    //
    // Description
    // Returns the magic matrix of size n.
    // The sum over the rows or the sum over the columns
    // is always the same.
    // This is a port of Scilab's magic.f
    //
    // The algorithm is different if n is a multiple of 4, an even
    // or odd number.
    //
    // Examples
    // A = makematrix_magic ( 5 )
    // // Check that it is "magic"
    // sum(A,"r")
    // sum(A,"c")
	//
    // A = makematrix_magic ( 4 )
    //
    // Authors
	// Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //   "Mathematical recreations and essays", 12th ed., by w. w. Rouse Ball and h. s. m. Coxeter
    //

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_magic" , rhs , 1 : 1 )
    apifun_checklhs ( "makematrix_magic" , lhs , 1 : 1 )
	//
    apifun_checktype ( "makematrix_magic" , n , "n" , 1 , "constant" )
	//
    apifun_checkscalar ( "makematrix_magic" , n , "n" , 1 )
	//
    apifun_checkflint ( "makematrix_magic" , n , "n" , 1 )
	apifun_checkgreq ( "makematrix_magic" , n , "n" , 1 , 1 )
    //
    A = zeros(n,n)
    if modulo(n,4) == 0 then
        //
        // n is a multiple of 4
        //
        k = 1
        for i = 1:n
            for j = 1:n
                A(i,j) = k
                if int(modulo(i,4)/2) == int(modulo(j,4)/2) then
                    A(i,j) = n*n+1 - k
                end
                k = k+1
            end
        end
    else
        //
        // n odd or even
        //
        if modulo(n,2) == 0 then
            m = n/2
        else
            m = n
        end
        i = 1
        j = (m+1)/2
        mm = m*m
        for k = 1: mm
            A(i,j) = k
            i1 = i-1
            j1 = j+1
            if i1 < 1 then
                i1 = m
            end
            if j1 > m then
                j1 = 1
            end
            if int(A(i1,j1))<>0 then
                i1 = i+1
                j1 = j
            end
            i = i1
            j = j1
        end
        if modulo(n,2) <> 0 then
            return
        end
        //
        //     rest of even order
        //
        t = m*m
        for i = 1: m
            for j = 1: m
                im = i+m
                jm = j+m
                A(i,jm) = A(i,j) + 2*t
                A(im,j) = A(i,j) + 3*t
                A(im,jm) = A(i,j) + t
            end
        end
        m1 = (m-1)/2
        if m1==0 then
            return
        end
        for j = 1:m1
            // Swap A(1:m,j) and A(m+1:m+m,j)
            t = A(1:m,j)
            A(1:m,j) = A(m+1:m+m,j)
            A(m+1:m+m,j) = t
        end
        m1 = (m+1)/2
        m2 = m1 + m
        // Swap A(m1,1) and A(m2,1)
        t = A(m1,1)
        A(m1,1) = A(m2,1)
        A(m2,1) = t
        // Swap A(m1,m1) and A(m2,m1)
        t = A(m1,m1)
        A(m1,m1) = A(m2,m1)
        A(m2,m1) = t
        m1 = n+1-(m-3)/2
        if m1 > n then
            return
        end
        for j = m1: n
            t = A(1:m,j)
            A(1:m,j) = A(m+1:m+m,j)
            A(m+1:m+m,j) = t
        end
    end
endfunction

