// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = makematrix_vandermonde ( varargin )
    //   Returns the Vandermonde matrix
    //
    // Calling Sequence
    //   A = makematrix_vandermonde ( x )
    //   A = makematrix_vandermonde ( x , n )
    //
    // Parameters
    //  x : a m-by-1 or 1-by-m matrix of doubles
    //  n : a 1-by-1 matrix of doubles, integer value
    //  A : a m-by-n matrix of doubles, the Vandermonde matrix of x.
    //
    // Description
    //   Returns the Vandermonde matrix of size n,
    //   made with entries from x.
    //
	//   For i = 1, 2, ..., m and j = 1, 2, ..., n, we have
    //   <literal>
    //   A(i,j) = x(i)^(j-1)
    //   </literal>
	//
	// The determinant of Vandermonde's matrix is
    //
    //   <latex>
    //   \det(A) = \prod_{1\leq i<j\leq n} (x_j-x_i).
    //   </latex>
	//
	// Caution: in Matlab, the columns produced by the
	// "vander" function are reversed.
    //
    // Examples
    //  A = makematrix_vandermonde ( 1:5 )
	// // Get only the first 3 columns, i.e.
	// // up to power 2.
    //  A = makematrix_vandermonde ( 1:5 , 3 )
	//
	// // Compare exact determinant with Scilab's det
	// n = 5;
	// x = (1:n)';
	// A = makematrix_vandermonde ( x )
	// // Compute exact determinant
	// d = 1.;
	// for i = 1: n
	// for j = i+1: n
	// d = d * (x(j) - x(i));
	// end
	// end
	// disp([det(A) d])
    //
    // Authors
    //  Copyright (C) 2011 - Michael Baudin
    //  Copyright (C) 2011 - DIGITEO - Michael Baudin
    //  Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //
    // Bibliography
    //   http://en.wikipedia.org/wiki/Vandermonde_matrix
    //

	[lhs, rhs] = argn()
    apifun_checkrhs ( "makematrix_vandermonde" , rhs , 1 : 2 )
    apifun_checklhs ( "makematrix_vandermonde" , lhs , 1 : 1 )
    //
	x = varargin(1)
	n = apifun_argindefault ( varargin , 2 , size(x,"*") )
	//
    if (x==[]) then
        A = []
        return
    end
	//
    apifun_checktype ( "makematrix_vandermonde" , x , "x" , 1 , "constant" )
    apifun_checktype ( "makematrix_vandermonde" , n , "n" , 2 , "constant" )
	//
    apifun_checkvector ( "makematrix_vandermonde" , x , "x" , 1 )
    apifun_checkscalar ( "makematrix_vandermonde" , n , "n" , 2 )
	//
    apifun_checkflint ( "makematrix_vandermonde" , n , "n" , 2 )
	apifun_checkgreq ( "makematrix_vandermonde" , n , "n" , 2 , 1 )
	//
    x = x(:)
    p = 0:n-1
	m = size(x,"*")
    p = p(ones(1,m),:)
    x = x(:,ones(n,1))
    A = x.^p
endfunction

