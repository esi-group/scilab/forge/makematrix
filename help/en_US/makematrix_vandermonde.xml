<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from makematrix_vandermonde.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="makematrix_vandermonde" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>makematrix_vandermonde</refname><refpurpose>Returns the Vandermonde matrix</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   A = makematrix_vandermonde ( x )
   A = makematrix_vandermonde ( x , n )

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x :</term>
      <listitem><para> a m-by-1 or 1-by-m matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>n :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value</para></listitem></varlistentry>
   <varlistentry><term>A :</term>
      <listitem><para> a m-by-n matrix of doubles, the Vandermonde matrix of x.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Returns the Vandermonde matrix of size n,
made with entries from x.
   </para>
   <para>
For i = 1, 2, ..., m and j = 1, 2, ..., n, we have
<literal>
A(i,j) = x(i)^(j-1)
</literal>
   </para>
   <para>
The determinant of Vandermonde's matrix is
   </para>
   <para>
<latex>
\det(A) = \prod_{1\leq i&lt;j\leq n} (x_j-x_i).
</latex>
   </para>
   <para>
Caution: in Matlab, the columns produced by the
"vander" function are reversed.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
A = makematrix_vandermonde ( 1:5 )
// Get only the first 3 columns, i.e.
// up to power 2.
A = makematrix_vandermonde ( 1:5 , 3 )

// Compare exact determinant with Scilab's det
n = 5;
x = (1:n)';
A = makematrix_vandermonde ( x )
// Compute exact determinant
d = 1.;
for i = 1: n
for j = i+1: n
d = d * (x(j) - x(i));
end
end
disp([det(A) d])

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2011 - Michael Baudin</member>
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   <member>Copyright (C) 2008-2009 - INRIA - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>http://en.wikipedia.org/wiki/Vandermonde_matrix</para>
</refsection>
</refentry>
